If you put your exported structure folder in a default profile then 
the content will automatically be imported by generic setup when the related package is installed.

To avoid this you can put your structure folder of exported content into this folder.
You can then 'manually' install it, and specify where it goes with the ContentMigrator tool
by selecting 'Content Migrator Tool Import' from the profiles drop down.
