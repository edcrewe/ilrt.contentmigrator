# Quick way to separate out config - ie as python file
SOURCE = 'myname-ploneimport-v1'
SITE = 'ploneimport'
USER = 'myname@gmail.com'
PW = 'mypassword'
EXPORT_FILES = '/path/to/plone/var/structure'
IMPORT_FILES = '/path/to/plone/ilrt/contentmigrator/profiles/import/structure'
