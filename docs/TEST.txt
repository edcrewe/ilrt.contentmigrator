Running the test suite
======================

Dependencies
------------

- The python used requires python-profiler to be installed so import profile will be OK
- Run bin buildout with the following test section in parts to build the test runner

[test]
recipe = zc.recipe.testrunner
defaults = ['--auto-color', '--auto-progress']
eggs = 
    ${instance:eggs}

- This generates bin/test in the buildout generated instance directories, run via

bin/test -c -x -1 ilrt.contentmigrator

- for example to get coloured output from the tests and stop on the first test
